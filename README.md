# Count titles in your file

Clone the repository and run the main.py file in your terminal.
Input the filename.
The program will print how many titles you have in how many lines.

HTML, Markdown, reStructuredText are automatically supported.
You can also check files of other types, but you need to provide the regex for it (inside the terminal).
