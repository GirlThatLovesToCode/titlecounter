import re
import pathlib
from collections import Counter

document = input('Enter file: ')

if len(document) < 1:
    document = "test.md"

# create the regex based on file type
file_suffix = pathlib.Path(document).suffix.lower()[1:]
if file_suffix == "mkd" or file_suffix == "md":
    regex = r"[#]+"
elif file_suffix == "html":
    regex = r"<h[1-6]>"
elif file_suffix == "rst" or file_suffix == "rest":
    # regex = r"=\n"
    regex = r"=\n|-\n"
else:
    regex = input('Enter regex: ')

# count lines and title occurrences
matches = []
line_num = 0
with open(document, "r") as file:
    lines = file.readlines()

    for line in lines:
        matches.extend(re.findall(regex, line, re.IGNORECASE))
        line_num += 1

title_num = Counter(matches)
print(title_num, "titles counted in a file that is ", line_num, "lines long.")
