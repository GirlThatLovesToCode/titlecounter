Duis dapibus purus libero, et interdum ipsum lobortis a. 
Phasellus et iaculis enim, sed interdum elit. In hac habitasse platea dictumst. Mauris aliquet elit vitae eros tincidunt, vitae tincidunt risus ullamcorper. Nam consectetur, velit in pretium hendrerit, elit velit pharetra mauris, id eleifend nunc eros vel nulla. Donec finibus gravida quam in convallis. Vestibulum consectetur orci libero, vestibulum auctor mauris rutrum non.

## Limit the decisions

Nam fermentum facilisis lorem. 
Sed consequat mi dapibus fermentum ultrices. 
Fusce lacinia justo dolor, in lobortis leo placerat id. 
Ut tempus nisi vestibulum pretium dignissim. 
Nullam ornare ornare nisl, eget gravida quam feugiat ac. 
Donec iaculis turpis interdum lacus porttitor ullamcorper.

### Limit the need for making the good choices

#### Routine

Nam nec lectus et turpis tempus ultrices ac eu turpis. Maecenas hendrerit odio sed ante interdum fermentum. 
Cras porta, libero quis efficitur gravida, nisi tortor tristique ipsum, id egestas velit felis sed sapien. Praesent mollis auctor ante et ultrices.
Curabitur quam arcu, cursus ac justo vel, tempor iaculis est.
Mauris aliquet lacus sed mauris aliquet, a pulvinar mauris cursus.
Nam malesuada metus vel neque blandit, at facilisis quam varius. Nunc leo diam, interdum sit amet eleifend vel, tempor vitae sem.

#### Plan ahead
Nullam malesuada sodales enim. 
Pellentesque non viverra enim. 
Proin mattis nisl sed orci pretium suscipit. 
Nulla quis aliquam magna. 
Praesent iaculis, urna eget viverra consectetur, mauris ex rutrum nulla, accumsan interdum augue ex efficitur tellus. 
Morbi convallis tortor ante, non ullamcorper mi pulvinar eget. 
Pellentesque lobortis iaculis nibh suscipit consequat.


### Make unwanted choices harder to make

Cras nec ex a risus scelerisque tempus sed at erat. 
Proin condimentum tellus et quam facilisis elementum. 
Duis suscipit ante eu risus lobortis fringilla. 
Fusce quis justo nisi. 


## Have a vision

Vestibulum suscipit nibh nunc. 
Maecenas dapibus augue vel mi pellentesque, at pharetra nunc faucibus. 
Pellentesque gravida nunc ex, eu imperdiet ligula interdum nec. 
Duis maximus congue enim sed venenatis. 
Integer dictum tortor eget tortor porttitor auctor. 
Donec bibendum malesuada arcu, rutrum laoreet est ullamcorper eu. 
Suspendisse vel urna orci. Nullam vitae tincidunt leo, ut lobortis ipsum. 

## Have a vision 2

Aenean ut justo eleifend, rutrum est sed, varius risus. 
Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
Vestibulum vitae libero ligula. Mauris sed felis eu sem volutpat dapibus. 
Etiam molestie molestie turpis, eget eleifend ipsum tincidunt luctus. 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                  





